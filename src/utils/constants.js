export const INITIAL_VALUE = {
    PRODUCT: {
        "fund_code": "",
        "fund_name": "",
        "totalMinimalSubscribe": "",
        "totalMinimalTopup": "",
        "fundFactSheet_id": "",
        "fundFactSheetUrl": "",
        "prospectus_id": "",
        "prospectusUrl": "",
        "allocation_stock": "",
        "allocation_forex": "",
        "allocation_obligation": "",
        "prefix": "",
        //object file
        "prospectusFile" : "",
        "fundFactSheetFile" : ""
    },
    DASHBOARD_INFO: {
        "totalTransactionSub": {
            "total_sub": 0
        },
        "totalTransactionRedeem": {
            "total_redeem": 0
        },
        "totalRegistration": {
            "total_reg": 0
        },
        "totalProduct": {
            "total_prod": 0
        },
    },
    ERR_PRODUCT: {
        "fundCodeErr": "",
        "fundNameErr": "",
        "totalMinSubsErr": "",
        "totalMinTopupErr": "",
        "factSheetIdErr": "",
        "factSheetUrlErr": "",
        "prospectusIdErr": "",
        "prospectusUrlErr": "",
        "allocStockErr": "",
        "allocForexErr": "",
        "allocObligationErr": "",
        "prefixErr": "",
        "prospectusFileErr": "",
        "fundFactSheetFileErr": ""
    },
    CUR_VALUE: {
        "totalMinimalSubscribe" : "",
        "totalMinimalTopup": ""
    }
}