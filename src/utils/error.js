export const newFieldRequiredError = (field) => `${field} should not be empty`

export const newFieldValueError = (field) => `${field} value is not valid`

export const newMustFilledFirstError = (field) => `${field} should be filled first`

export const newInvalidTotalAllocation = () => 'total fund allocation should be 100'

export const newInvalidPrefixLength = () => 'prefix should have maximum 2 characters'

export const newInvalidTotalMinimum = (field) => `${field} should not negative value`