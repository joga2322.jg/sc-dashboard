import { useNavigate } from "react-router-dom";

const useLogout = () => {
    const navigate = useNavigate()

    const onLogout = () =>{
        console.log('logout');
        localStorage.removeItem('token');
        navigate("/", {replace: true});
    }

    return {
        onLogout
    };
}
 
export default useLogout;