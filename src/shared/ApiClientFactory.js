const apiClientFactory = (client) => {
    const doPost = async ({url='', data=null}) => {
        try {
            const config = {headers : {Authorization: `Bearer ${localStorage.getItem('token')}`}}
            const response = await client.post(url, data, config)
            if (response.data.responseCode !== '2000000'){
                throw response.data.responseMessage
            }
            return response.data
        } catch (error) {
            throw error
        }
    }

    const doGet = async ({url=''}) => {
        try {
            const config = {headers : {Authorization: `Bearer ${localStorage.getItem('token')}`}}
            const response = await client.get(url, config)
            if (response.data.responseCode !== '2000000'){
                throw response.data.responseMessage
            }
            return response.data
        } catch (error) {
            throw error
        }
    }

    const doPut = async ({url='', data=null}) => {
        try {
            const config = {headers : {Authorization: `Bearer ${localStorage.getItem('token')}`}}
            const response = await client.put(url, data, config)
            if (response.data.responseCode !== '2000000'){
                throw response.data.responseMessage
            }
            return response.data
        } catch (error) {
            throw error
        }
    }

    const doDelete = async ({url=''}) => {
        try {
            const config = {headers : {Authorization: `Bearer ${localStorage.getItem('token')}`}}
            const response = await client.delete(url, config)
            if (response.data.responseCode !== '2000000'){
                throw response.data.responseMessage
            }
            return response.data
        } catch (error) {
            throw error
        }
    }

    return{
        doPost,
        doGet,
        doPut,
        doDelete
    }
}

export default apiClientFactory
