import { useEffect } from 'react';
import '../../../assets/css/buttons.css'
import "../../../assets/css/upload-view.css"

// classname jadi props

export const ButtonPrimary = (props) => {
    const {label, fnOnClick} = props
    return ( 
        <button className='btn btn-primary' onClick={()=>{fnOnClick()}}>{label}</button>
    );
}

export const ButtonPrimaryHeightAuto = (props) => {
    const {label, fnOnClick} = props
    return ( 
        <button className='btn-auto btn-primary' onClick={()=>{fnOnClick()}}>{label}</button>
    );
}

export const ButtonPrimaryGrad = (props) => {
    const {label, fnOnClick} = props
    return ( 
        <button className='btn btn-primary-gradient' onClick={()=>{fnOnClick()}}>{label}</button>
    );
}

export const ButtonSecondary = (props) => {
    const {label, fnOnClick} = props
    return ( 
        <button className='btn btn-secondary' onClick={()=>{fnOnClick()}}>{label}</button>
    );
}

export const ButtonTertiary = (props) => {
    const {label, fnOnClick} = props
    return ( 
        <button className='btn btn-tertiary' onClick={()=>{fnOnClick()}}>{label}</button>
    );
}

export const ButtonNavbar = (props) => {
    // How to Use
    // <ButtonNavbar imgSrc={Dashboard} label='Dashboard' fnOnClick={functionName}/>
    const {imgSrc, label, isClicked} = props
    return (
        <button className='btn-navbar-menu'>
            {isClicked ? <div className='btn-marker-show'/> : <div className='btn-marker'/>}
            <img className='btn-menu-icon' src={imgSrc}/>
            <span className='btn-menu-caption'>{label}</span>
        </button>
    );
}

export const ButtonLogout = (props) => {
    // How to Use
    // <ButtonLogout imgSrc={Dashboard} label='Dashboard' fnOnClick={functionName}/>
    const {imgSrc, label, fnOnClick} = props
    return (
        <button className='btn-navbar-menu btn-logout' onClick={()=>{fnOnClick()}}>
            <div className='btn-marker'/>
            <img className='btn-menu-icon' src={imgSrc}/>
            <span className='btn-menu-caption'>{label}</span>
        </button>
    );
}

export const ButtonIcon = (props) => {
    // How to Use
    // <ButtonIcon imgSrc={Dashboard} fnOnClick={functionName}/>
    const {imgSrc, fnOnClick} = props
    return (
        <button className='btn-icon' onClick={()=>{fnOnClick()}}>
            {imgSrc.includes('.') ?
                <img src={imgSrc}/> : <>{imgSrc}</>
            }
        </button>
    );
}

export const ButtonUploadType = (props) => {
    const {imgSrc, message, isClicked, setClicked, setChoice} = props

    const updateClicked = () => {
        isClicked[2].fill(false)
        isClicked[2][isClicked[0]] = !isClicked[1]
        setClicked([...isClicked[2]])
        setChoice(isClicked[0])
    }
        
    return (
        <>
            {isClicked[1] ? <button className="card-upload-type glow" onClick={()=>{updateClicked()}}>
                <div className="card-type-icon">
                    <img src={imgSrc}/>
                </div>
                <span className="h6-type color-grey">{message[0]}</span>
                <span className="body2-type color-light-grey">Select this option to upload <br/>{message[1]}</span>
            </button> : 
            <button className="card-upload-type" onClick={()=>{updateClicked()}}>
                <div className="card-type-icon">
                    <img src={imgSrc}/>
                </div>
                <span className="h6-type color-grey">{message[0]}</span>
                <span className="body2-type color-light-grey">Select this option to upload <br/>{message[1]}</span>
            </button>
            }
        </>
    );
}