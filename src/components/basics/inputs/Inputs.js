import '../../../assets/css/inputs.css'
import '../../../assets/css/container.css'
import '../../../assets/css/font-style.css'

import * as React from 'react';
import Box from '@mui/material/Box';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DateRangePicker } from '@mui/x-date-pickers-pro/DateRangePicker';
import { ButtonIcon, ButtonPrimary } from '../buttons/Buttons';
import Remove from '../../../assets/img/icon-garbage.svg'

export const TextInput = (props) => {
    // How to use
    // <TextInput label='Username' name='username' fnOnChange={setValue}/>
    const {label, name, fnOnChange, errorMsg, defVal="", readOnly=false} = props
    return ( 
        <>
            {readOnly ? 
            <div className='input-container'>
                <label className='input-label'>{label}</label>
                <input className='input-disabled' type='text' onChange={(e)=>{fnOnChange(e.target.name, e.target.value)}} id={name} name={name} defaultValue={defVal} disabled/>
                {errorMsg ? <span className='input-error'>{errorMsg}</span> :<div></div>}
            </div> :
            <div className='input-container'>
                <label className='input-label'>{label}</label>
                <input className='input-text-box' type='text' onChange={(e)=>{fnOnChange(e.target.name, e.target.value)}} id={name} name={name} defaultValue={defVal} readOnly={readOnly}/>
                {errorMsg ? <span className='input-error'>{errorMsg}</span> :<div></div>}
            </div>
            }
            
        </>
    );
}

export const NumberInput = (props) => {
    // How to use
    // <NumberInput label='Total' name='total' fnOnChange={setValue}/>
    const {label, name, fnOnChange, errorMsg, defVal="", value} = props
    return ( 
        <div className='input-container'>
            <label className='input-label'>{label}</label>
            <input className='input-text-box' type='number' onChange={(e)=>{fnOnChange(e.target.name, e.target.value)}} id={name} name={name} placeholder="0" value={value} min="0"/>
            {errorMsg ? <span className='input-error'>{errorMsg}</span> :<div></div>}
        </div>
    );
}

export const CurrecyInput = (props) => {
    // How to use
    // <NumberInput label='Total' name='total' fnOnChange={setValue}/>
    const {label, name, fnOnChange, errorMsg, defVal="", value, currencyVal} = props
    return ( 
        <div className='input-container'>
            <label className='input-label'>{label}</label>
            <input className='input-text-box' type='number' onChange={(e)=>{fnOnChange(e.target.name, e.target.value)}} id={name} name={name} placeholder="0" value={value} min="0"/>
            {errorMsg ? <span className='input-error'>{errorMsg}</span> : <span className='input-label color-light-grey'>{currencyVal}</span>}
        </div>
    );
}

export const PasswordInput = (props) => {
    // How to use
    // <PasswordInput label="Password" name="password" fnOnChange={setValue}/>
    const {label, name, fnOnChange, errorMsg} = props
    return ( 
        <div className='input-container'>
            <label className='input-label'>{label}</label>
            <input className='input-text-box' type='password' onChange={(e)=>{fnOnChange(e.target.value)}} id={name} name={name}/>
            {errorMsg ? <span className='input-error'>{errorMsg}</span> :<div></div>}
        </div>
    );
}

export const DropdownInput = (props) => {
    // How to use
    // <DropdownInput label='Username' name='username' fnOnChange={setValue} options={['pepaya','mangga','pisang']}/>
    const {label, name, fnOnChange, options} = props
    return ( 
        <div className='input-container'>
            <label className='input-label'>{label}</label>
            <select className='input-text-box' id={name} name={name} onChange={(e)=>{fnOnChange(e.target.value)}}>
                {options ?
                options.map((opt)=><option>{opt}</option>):
                <option selected disabled>Pilihan Kosong</option>
                }
            </select>
        </div>
    );
}

export const DateInput = (props) => {
    // How to use
    // <DateInput label='Username' name='username' fnOnChange={setValue}/>
    const {label, name, fnOnChange, errorMsg} = props
    return ( 
        <div className='input-container'>
            <label className='input-label'>{label}</label>
            <input className='input-text-box' type='text' onChange={(e)=>{fnOnChange(e.target.value)}} id={name} name={name}/>
            {errorMsg ? <span className='input-error'>{errorMsg}</span> :<div></div>}
        </div>
    );
}

export const CustomDateRangeInputs = (props) => {
    const [value, setValue] = React.useState([null, null]);
    const {fnOnChange, fnApplyFilter} = props
  
    return (
        <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DateRangePicker
                label="date-filter"
                inputFormat="dd/MM/yyyy"
                value={value}
                maxDate={new Date()}
                onChange={(newValue) => {
                    setValue(newValue)
                    fnOnChange(newValue)
                }}
                renderInput={(startProps, endProps) => (
                    <React.Fragment>
                        <div className='flex-container flex-row content-end gap-24'>
                            <div className='date-input-container w-40'>
                                <label className='input-label'>Start Date</label>
                                <input className='input-text-box' ref={startProps.inputRef} {...startProps.inputProps} />
                            </div>

                            <div className='date-input-container w-40'>
                                <label className='input-label'>End Date</label>
                                <input className='input-text-box' ref={endProps.inputRef} {...endProps.inputProps} />
                            </div>

                            <div className='date-input-container w-20'>
                            <ButtonPrimary label='Apply' fnOnClick={() => fnApplyFilter(value)}/>
                            </div>
                        </div>
                    </React.Fragment>
                )}
            />
        </LocalizationProvider>
    );
  }

export const FileUploaderForProduct = (props) => {
    const {label, errorMsg, file, setFile, fileName, disable="false"} = props
    return ( 
        <>
        {file === ""? 
        <div className='input-container'>
            <label className='input-label'>{label}</label>
            {disable === "true"? 
            <div className='input-banner-disabled'>
                <span className='input-label'>{errorMsg}</span>
            </div>
            :
            <>
                <input type="file" id="file-1" className="inputfile inputfile-1" style={{display:"none"}} onChange={(e)=>{setFile(prevState => ({...prevState, [`${fileName}`]:e.target.files[0]}))}}/>
                <label htmlFor="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a File</span></label> 
                {errorMsg ? 
                <span className='input-error'>{errorMsg}</span> :
                <></>
            }
            </>
            }
            
            
        </div> :
        <div className='input-container'>
            <label className='input-label'>{label}</label>
            <div className='uploaded-file-detail-container stripped'>
                <div className='flex-container flex-column gap-4'>
                    <span className='label-input color-grey'>{file.name}</span>
                    <span className='hint-type color-light-grey'>Size: <span className='color-grey' style={{fontWeight:"500"}}>{file.size} bytes</span></span>
                </div>
                <button className='btn-icon' onClick={()=>{setFile(prevState => ({...prevState, [`${fileName}`]:""}))}}>
                    <img src={Remove} style={{"width":"20px","height":"20px"}}/>
                </button>
            </div>
        </div>
        }
        </>
        
    );
}