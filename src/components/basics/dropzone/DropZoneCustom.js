import React, {useEffect, useMemo} from 'react';
import {useDropzone} from 'react-dropzone';
import Upload from '../../../assets/img/icon-upload.svg'
import '../../../assets/css/font-style.css'
import '../../../assets/css/container.css'
import '../../../assets/css/badge.css'
import iconFile from '../../../assets/img/icon-files.svg'

const baseStyle = {
    "boxSizing":"border-box",
    "display":"flex",
    "flexDirection":"column",
    "justifyContent":"center",
    "alignItems":"center",
    "padding":"20px",
    "gap":"20px",
    "width":"100%",
    "height":"auto",
    "background":"#FFECE6",
    "border":"2px dashed rgba(58, 53, 65, 0.38)","borderRadius":"10px"
};

const focusedStyle = {
    borderColor: '#2196f3'
};

const DropZoneCustom = (props) => {
    const {choice, onDrop} = props
    var fileTypes = {}
    var fileDetail = []
    var acceptedFileExtention = ''

    if (choice === 0 || choice === 3) {
        fileTypes = {
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' : []
        }
        acceptedFileExtention = '.xlsx'
    } else {
        fileTypes = {
            'text/plain': []
        }
        acceptedFileExtention = '.txt'
    }

    const {
        acceptedFiles,
        fileRejections,
        getRootProps,
        getInputProps,
        isFocused,
    } = useDropzone({
        accept: fileTypes, maxFiles:1, onDrop: onDrop});
        
    if (acceptedFiles.length !== 0) {
        fileDetail.push({
            path: acceptedFiles[0].path, 
            size : acceptedFiles[0].size, 
            message: "Upload Ready"
        })
    } else if (fileRejections.length !== 0) {
        for (let i = 0; i < fileRejections.length; i++){
            if (fileRejections[i].errors[0].code === "file-invalid-type") {
                fileDetail.push({
                    path: fileRejections[i].file.path, 
                    size : fileRejections[i].file.size, 
                    message: "Invalid file extention"
                })
            } else if (fileRejections[i].errors[0].code === "too-many-files") {
                fileDetail.push({
                    path: fileRejections[i].file.path, 
                    size : fileRejections[i].file.size, 
                    message: "You can only upload one file"
                })
            }
        }
    }

    const fileCheckResult = fileDetail.map(file => { 
        return (
          <div className='uploaded-file-detail-container' key={file.path}>
                <img src={iconFile} style={{"width":"24px","height":"24px"}}/>
                <div className='flex-container flex-column gap-4'>
                    <span className='body2-semibold-type color-grey'>{file.path}</span>
                    <span className='label-input color-light-grey'>Size: <span className='color-grey'>{file.size} bytes</span></span>
                </div>
                <div style={{width:'40%'}}>
                    {file.message === "Upload Ready" ? 
                    <span className='badge success body2-type'>{file.message}</span>:
                    <span className='badge error body2-type'>{file.message}</span>}
                </div>
          </div>
        ) 
    });

    const style = useMemo(() => ({
        ...baseStyle,
        ...(isFocused ? focusedStyle : {}),
      }), [
        isFocused,
      ]);

    return ( 
        <div className="flex-container flex-column gap-24">
            <div {...getRootProps({style})}>
                <input {...getInputProps()} />
                <img src={Upload} style={{"width":"64px","height":"64px"}}/>

                <div className='flex-container-center flex-column gap-4'>
                    <span className='h4-type color-grey'>Drag & Drop</span>
                    <span className='label-input color-extra-light-grey'>Your file here, or click to browse</span>
                </div>
                
                <span className='hint-type color-extra-light-grey'>Accepted file extention: <span className='label-input color-grey'>{acceptedFileExtention}</span></span>
            </div>
            <div style={{width: "100%"}}>
                {fileCheckResult}
            </div>
        </div>
    );
}
 
export default DropZoneCustom;