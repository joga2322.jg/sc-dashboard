import "../../../assets/css/container.css"
import "../../../assets/css/font-style.css"
import "../../../assets/css/banner.css"
import fileIllustration from "../../../assets/img/missing-file-illustration.png"
import { ButtonPrimaryHeightAuto } from "../buttons/Buttons"
import Success from "../../../assets/img/icon-sucess-notif.svg"

export const BannerButtonWithIllustration = () => {
    return (  
        <div className="banner-image-container">
            <div className="flex-container flex-column gap-20" style={{alignSelf:"stretch"}}>
                <div className="flex-container flex-column gap-8">
                    <span className="h5-type color-grey">Ay Yoo, Friends! <span className="color-sc-blue-1">Something’s MISSING!</span></span>
                    <span className="label-input color-light-grey">The daily NAV file is not uploaded yet. Please hit the button bellow to upload the file. Cheers! </span>
                </div>
                <ButtonPrimaryHeightAuto label='Upload NAV File'/>
            </div>
            
            <div className="flex-container-center-end flex-column w-25">
                <img src={fileIllustration} alt="" style={{height:"80%", maxHeight:"100px", marginRight:"25%",}}/>
            </div> 
        </div>
    );
}

export const BannerNAVUploaded = () => {
    return (  
        <div className="banner-nav-uploaded">
            <div className="banner-icon">
                <img className="" src={Success} alt=""/>
            </div>
            <span className="body2-type color-light-grey"><span className="body2-semibold-type color-sc-blue-1">Way to Go! </span>The Daily NAV has been uploaded for today</span>
        </div>
    );
}