import "../../../assets/css/container.css"
import loading from "../../../assets/img/icon-loading.svg"
import Logo from '../../../assets/img/sc-logo-white.svg'

const LoadingScreen = () => {
    return ( 
        <div className="loading-screen-container">
            <div className="flex-container-center flex-column height-100vh gap-24">
                <img src={Logo} style={{height:"120px"}}/>
                <img src={loading} style={{height:"44px"}}/>
            </div>
        </div>
    );
}
 
export default LoadingScreen;