import "../../../assets/css/notification-bar.css"
import "../../../assets/css/container.css"
import { ButtonIcon } from "../../basics/buttons/Buttons";
import CloseBlue from "../../../assets/img/icon-close-blue-16px.svg"
import CloseRed from "../../../assets/img/icon-close-red-16px.svg"
import Success from "../../../assets/img/icon-sucess-notif.svg"
import Failure from "../../../assets/img/icon-failure-notif.svg"

export const SuccessNotificationExample = () => {
    return ( 
        <div class="tn-box tn-box-color-1">
            <p>Your settings have been saved successfully!</p>
            <div class="tn-progress"></div>
        </div>
    );
}

export const SuccessNotification = (props) => {
    const {fnOnClick, autoClose = false, message} = props
    return ( 
        <>
        {autoClose === true ? 
        <div className="notif-box success-notif box-fade-out-4sec">
            <div className="flex-container flex-column">
                <div className="flex-container-center flex-row gap-16">
                    <img src={Success} alt=""/>
                    <div className="flex-container flex-column gap-4">
                        <span className="body1-semibold-type color-sc-blue-2">Success!</span>
                        <span className="label-input color-grey">{message}</span>
                    </div>
                    <ButtonIcon imgSrc={CloseBlue} fnOnClick={()=>{fnOnClick()}}/>
                </div>
            </div>
            <div className="flex-container flex-column progress-bar-success-bg">
                <div className="success-progressing-bar progress-3sec"></div>
            </div>
        </div>:
        <div className="notif-box success-notif box-fade-in">
            <div className="flex-container flex-column">
                <div className="flex-container-center flex-row gap-16">
                    <img src={Success} alt=""/>
                    <div className="flex-container flex-column gap-4">
                        <span className="body1-semibold-type color-sc-blue-2">Success!</span>
                        <span className="label-input color-grey">{message}</span>
                    </div>
                    <ButtonIcon imgSrc={CloseBlue} fnOnClick={()=>{fnOnClick()}}/>
                </div>
            </div>
        </div>
        }
        </>
    );
}

export const ErrorNotification = (props) => {
    const {fnOnClick, autoClose = false, message} = props
    console.log(autoClose);
    return ( 
        <>
        {autoClose === true ? 
        <div className="notif-box error-notif box-fade-out-4sec">
            <div className="flex-container flex-column">
                <div className="flex-container-center flex-row gap-16">
                    <img src={Failure} alt=""/>
                    <div className="flex-container flex-column gap-4">
                        <span className="body1-semibold-type color-error-medium">Oh Snap!</span>
                        <span className="label-input color-grey">{message}</span>
                    </div>
                    <ButtonIcon imgSrc={CloseRed} fnOnClick={()=>{fnOnClick()}}/>
                </div>
            </div>
            <div className="flex-container flex-column progress-bar-error-bg">
                <div className="error-progressing-bar progress-3sec"></div>
            </div>
        </div> :
        <div className="notif-box error-notif box-fade-in">
            <div className="flex-container flex-column">
                <div className="flex-container-center flex-row gap-16">
                    <img src={Failure} alt=""/>
                    <div className="flex-container flex-column gap-4">
                        <span className="body1-semibold-type color-error-medium">Oh Snap!</span>
                        <span className="label-input color-grey">{message}</span>
                    </div>
                    <ButtonIcon imgSrc={CloseRed} fnOnClick={()=>{fnOnClick()}}/>
                </div>
            </div>
        </div>
        }
        </>        
    );
}