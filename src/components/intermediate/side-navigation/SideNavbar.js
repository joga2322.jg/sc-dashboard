import Logo from '../../../assets/img/sc-logo-color-one-line.svg'
import Dashboard from '../../../assets/img/icon-dashboard.svg'
import Product from '../../../assets/img/icon-mutual-fund.svg'
import Transaction from '../../../assets/img/icon-transaction.svg'
import Upload from '../../../assets/img/icon-upload.svg'
import Logout from '../../../assets/img/icon-garbage.svg'
import More from '../../../assets/img/icon-more.svg'
import PP from '../../../assets/img/profile-picture.png'
import { ButtonNavbar, ButtonLogout, ButtonIcon } from '../../basics/buttons/Buttons'
import "../../../assets/css/container.css"
import "../../../assets/css/navbar.css"
import useLogout from '../../../shared/hook/useLogout'
import {Link, useLocation} from "react-router-dom"
import { useEffect, useState } from "react";

const SideNavbar = () => {
    const { onLogout } = useLogout();
    let location = useLocation();
    const [isClicked, setClicked] = useState({
        dashboard : false,
        product: false,
        transaction : false,
        upload : false,
    })

    useEffect(()=>{
        setIsClicked(location.pathname)
    }, [])

    const setIsClicked = (page) => {
        if (page.includes('/dashboard')){
            setClicked({
                ...isClicked,
                dashboard : true,
            })
        } else if (page.includes('/product')) {
            setClicked({
                ...isClicked,
                product : true,
            })
        } 
        // else if (page.includes('/transaction')) {
        //     setClicked({
        //         ...isClicked,
        //         transaction : true
        //     })
        // } 
        else if (page.includes('/upload')) {
            setClicked({
                ...isClicked,
                upload: true
            })
        }
    }

    return ( 
        <div className="navbar-container w-20">
            <img className='navbar-logo' src={Logo}/>
            <div className='navbar-menu-group'>
                <span className='navbar-menu-group-title'>MENU</span>
                <Link to='/dashboard' style={{textDecoration: "none"}}>
                    <ButtonNavbar imgSrc={Dashboard} label='Dashboard'isClicked={isClicked.dashboard}/>
                </Link>
                <Link to='/product' style={{textDecoration: "none"}}>
                    <ButtonNavbar imgSrc={Product} label='Products' isClicked={isClicked.product}/>
                </Link>
                {/* <ButtonNavbar imgSrc={Transaction} label='Transactions' isClicked={isClicked.transaction}/> */}
                <Link to='/upload' style={{textDecoration: "none"}}>
                    <ButtonNavbar imgSrc={Upload} label='Upload' isClicked={isClicked.upload}/>
                </Link>
            </div>
            <div className='navbar-button-group'>
                <div className='card-profile'>
                    <img className='profile-picture' src={PP}/>
                    <div className='user-info-group'>
                        <span className='user-name'>Administrator</span>
                        <span className='user-role'>Administrator</span>
                    </div>
                    <ButtonIcon imgSrc={More}/>
                </div>
                <ButtonLogout imgSrc={Logout} label='Logout' fnOnClick={onLogout}/>
            </div>
        </div>
    );
}
 
export default SideNavbar;