import { useEffect, useLayoutEffect, useState } from "react";
import { useDeps } from '../../shared/DepContext'
import { useNavigate } from "react-router-dom"

const useLogin = () => {
    const { loginService } = useDeps();
    const[username, setUsername] = useState('')
    const[password, setPassword] = useState('')
    const[errorMsg, setErrorMsg] = useState({})
    const[isLoading, setLoading] = useState(false);
    //to check if login credetial correct or incorrect
    const[credStatus, setCredStatus] = useState({
        "error":false,
        "message": ""
    });
    //to check if any notif need to be shown
    const[isNotifReady,setNotif] = useState(false)
    const navigate = useNavigate();

    //function to close appearing notification
    const closeNotification = () => {
        setNotif(false)
    }

    useEffect(()=>{
        if (!username && !password) {
            setErrorMsg({
                ...errorMsg, 
                uNameErr: 'Username field should not be empty', 
                passErr: 'Password field should not be empty'
            })
        } else if (!username || !password) {
            if (!username) {
                setErrorMsg({
                    ...errorMsg, 
                    uNameErr: 'Username field should not be empty',
                    passErr: ''
                })
            } 

            if (!password) {
                setErrorMsg({
                    ...errorMsg, 
                    uNameErr: '',
                    passErr: 'Password field should not be empty'
                })
            } 
        } else {
            setErrorMsg({...errorMsg, passErr: '', uNameErr: ''})
        }
    }, [username, password])

    useEffect(() => {
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
        }, 1500);
    }, [])

    useLayoutEffect(() => {
        if (isLoading) {
          document.body.style.overflow = "hidden";
          document.body.style.height = "100%";
        }
        if (!isLoading) {
          document.body.style.overflow = "auto";
          document.body.style.height = "auto";
        }
    }, [isLoading]);
    
    const handleSubmit = async ()=>{
        let admin = {username:username, password: password};
        // hide any showing notification on screen
        setNotif(false)
        try {
            var response = await loginService.loginAdmin(admin);
            localStorage.setItem('token', response.access_token);

            //checking if daily nav has been uploaded in the day
            response = true
            localStorage.setItem('isNavUploaded', response);
            
            setCredStatus({
                "error": false,
                "message": "Welcome to the Dashboard!"
            })
            setTimeout(() => {
                navigate("/dashboard", {replace: true});
            }, 1500);
        } catch (error) {
            var message = ""
            if (error.message.includes("401")) {
                message = "Please input correct username or password"
            } else if (error.message.includes("500")) {
                message = "Server is temporary down, please try again later"
            }

            setCredStatus({
                "error": true,
                "message": message
            })
        } finally {
            // flag to show notification
            setNotif(true)
        }
    }

    return {
        setUsername, 
        setPassword, 
        errorMsg, 
        handleSubmit, 
        isLoading, 
        setLoading, 
        credStatus, 
        isNotifReady, 
        closeNotification
    }
}
 
export default useLogin;