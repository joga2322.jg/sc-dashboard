import "../../assets/css/container.css"
import "../../assets/css/login-view.css"
import Logo from '../../assets/img/sc-logo-white.svg'
import { ButtonPrimaryGrad } from "../../components/basics/buttons/Buttons";
import { PasswordInput, TextInput } from "../../components/basics/inputs/Inputs";
import LoadingScreen from "../../components/intermediate/loading-screen/LoadingScreen";
import { SuccessNotification, ErrorNotification} from "../../components/intermediate/notification-bar/NotificationBar";
import useLogin from "./useLogin";

const LoginView = () => {
    const{setUsername, setPassword, errorMsg, handleSubmit, isLoading, setLoading, credStatus, isNotifReady, closeNotification} = useLogin()

    return ( 
        <div className="flex-container flex-row">
            {isLoading ? <LoadingScreen/> : <></>}
            {isNotifReady ? credStatus.error ? <ErrorNotification message={credStatus.message} autoClose={true} fnOnClick={closeNotification}/> : <SuccessNotification message={credStatus.message} autoClose={false} fnOnClick={closeNotification}/> : <></>}
            <div className="login-container-left flex-row w-60">
                <div className="login-background">
                    <img className='login-sc-white-big' src={Logo}/>
                </div>
            </div>
            <div className="login-container-right flex-column w-40">
                <div className="form-login-container flex-column">
                    <span className="login-form-title">Login</span>
                    <TextInput label='Username' name='username' fnOnChange={(n,v)=>setUsername(v)} errorMsg={errorMsg.uNameErr}/>
                    <PasswordInput label='Password' name='password' fnOnChange={setPassword} errorMsg={errorMsg.passErr}/>
                    <ButtonPrimaryGrad label='Login' fnOnClick={() => {handleSubmit()}}/>
                </div>
            </div>
        </div>
    );
}
 
export default LoginView;