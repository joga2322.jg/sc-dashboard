import "../../assets/css/container.css"
import "../../assets/css/font-style.css"
import "../../assets/css/upload-view.css"
import SideNavbar from "../../components/intermediate/side-navigation/SideNavbar";
import { ButtonIcon, ButtonPrimary, ButtonSecondary, ButtonUploadType } from "../../components/basics/buttons/Buttons";
import Search from "../../assets/img/icon-search.svg"
import Notification from "../../assets/img/icon-notification.svg"
import Wallet from "../../assets/img/icon-wallet.svg"
import NAV from "../../assets/img/icon-diamond.svg"
import Register from "../../assets/img/icon-register.svg"
import Trx from "../../assets/img/icon-transaction.svg"
import DropZoneCustom from "../../components/basics/dropzone/DropZoneCustom";
import useUpload from "./useUpload";
import LoadingScreen from "../../components/intermediate/loading-screen/LoadingScreen";

const UploadView = () => {
    const{optionClicked, optionChoice, setOption, setChoice, isLoading, setLoading, onSubmitUpload, onDrop} = useUpload()

    return ( 
        <div className="view-container">
            {isLoading ? <LoadingScreen/> : <></>}
            <SideNavbar/>
            <div className="content-container w-80 padding-40">
                <div className="dashboard-property-bar">
                    <div className="flex-container flex-column gap-8">
                        <span className="h3-type color-grey">Upload</span>
                        <span className="body1-type color-extra-light-grey">
                            Here’s where you can upload important documents
                        </span>
                    </div>
                    <div className="flex-container-end flex-row gap-12">
                        <ButtonIcon imgSrc={Search}/>
                        <ButtonIcon imgSrc={Notification}/>
                    </div>
                </div>
                <div className="flex-container flex-column padding-24 gap-24 white card-elevation">
                    <div className="flex-container flex-column gap-4">
                        <span className="h5-type color-sc-blue-2">Step 1: Select Upload Type</span>
                        <span className="body2-type color-extra-light-grey">Please select upload type according to your situation</span>
                    </div>
                    <div className="flex-container flex-column gap-24">
                        <div className="flex-container flex-row gap-24">
                            <ButtonUploadType 
                                imgSrc={Wallet} 
                                message = {["Latest User Balance File","the latest user balance file"]}
                                isClicked = {[0, optionClicked[0], optionClicked]}
                                setClicked = {setOption}
                                setChoice = {setChoice}
                            />

                            <ButtonUploadType 
                                imgSrc={NAV} 
                                message = {["Latest NAV File","the latest NAV file"]}
                                isClicked = {[1, optionClicked[1], optionClicked]}
                                setClicked = {setOption}
                                setChoice = {setChoice}
                            />
                        </div>
                        <div className="flex-container flex-row gap-24">
                            <ButtonUploadType 
                                imgSrc={Register} 
                                message = {["Registration File","the registration file"]}
                                isClicked = {[2, optionClicked[2], optionClicked]}
                                setClicked = {setOption}
                                setChoice = {setChoice}
                            />
                            <ButtonUploadType 
                                imgSrc={Trx} 
                                message = {["Transaction File","the transaction file"]}
                                isClicked = {[3, optionClicked[3], optionClicked]}
                                setClicked = {setOption}
                                setChoice = {setChoice}
                            />
                        </div>
                    </div>

                    <div className="flex-container flex-column gap-4">
                        <span className="h5-type color-sc-blue-2">Step 2: Select File</span>
                        <span className="body2-type color-extra-light-grey">Please select corrrect file to be uploaded</span>
                    </div>

                    <div className="flex-container flex-column">
                        <div style={{width:'100%'}}>
                            <DropZoneCustom choice={optionChoice} onDrop={onDrop}/>
                        </div>
                    </div>

                    <div className="flex-container flex-column content-end gap-4">
                        <div className='linear-gradient'></div>
                        <div className='product-form-buttons-group'>
                            <ButtonSecondary label='Reset' fnOnClick={()=>window.location.reload()}/>
                            <ButtonPrimary label='Submit' fnOnClick={onSubmitUpload}/>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    );
}
 
export default UploadView;