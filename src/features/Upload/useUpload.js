import { useEffect, useLayoutEffect, useCallback, useState } from "react";
import { useDeps } from '../../shared/DepContext'

const useUpload = () => {
    const { uploadService } = useDeps();
    const [optionClicked, setOption] = useState([true,false,false,false])
    const [optionChoice, setChoice] = useState(0)
    const [uploadedFile, setUploadedFile] = useState()
    const [isLoading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
        }, 1500);
    }, [])

    useLayoutEffect(() => {
        if (isLoading) {
          document.body.style.overflow = "hidden";
          document.body.style.height = "100%";
        }
        if (!isLoading) {
          document.body.style.overflow = "auto";
          document.body.style.height = "auto";
        }
    }, [isLoading]);

    const onSubmitUpload = async () => {
        if (!uploadedFile){
            alert("You can only upload one file")
            return
        }
        setLoading(true)
        try {
            const data = new FormData()
            data.append("file", uploadedFile)
            
            let resp;
            switch (optionChoice) {
                case 0:
                    resp = await uploadService.uploadUserBalance(data)
                    break;
                case 1:
                    resp = await uploadService.uploadNav(data)
                    break;
                case 2:
                    resp = await uploadService.uploadRegistration(data)
                    break;
                case 3:
                    resp = await uploadService.uploadTransaction(data)
                    break;
                default:
                    break;
            }
            console.log(resp);
            alert('SUCCESS')
        } catch (error) {
            alert(error)
        } finally{
            setLoading(false)
        }
    }

    const onDrop = useCallback((acceptedFiles) => {
        acceptedFiles.map((file) => {
          const reader = new FileReader();
          reader.readAsDataURL(file);
          console.log("ini file",file);
          setUploadedFile(file)
          return file;
        });
      }, []);
    
    return {
        optionClicked,
        optionChoice,
        setOption,
        setChoice,
        isLoading, 
        setLoading,
        onSubmitUpload,
        onDrop
    }
}
 
export default useUpload;