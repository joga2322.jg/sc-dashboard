import "../../assets/css/container.css"
import "../../assets/css/font-style.css"
import "../../assets/css/dashboard-view.css"
import Search from "../../assets/img/icon-search.svg"
import Notification from "../../assets/img/icon-notification.svg"
import { ButtonIcon } from "../../components/basics/buttons/Buttons";
import SideNavbar from "../../components/intermediate/side-navigation/SideNavbar";
import { CustomDateRangeInputs } from "../../components/basics/inputs/Inputs"
import Transaction from '../../assets/img/icon-transaction.svg'
import Redeem from '../../assets/img/icon-dashboard.svg'
import Registrants from '../../assets/img/icon-active-investor.svg'
import Products from '../../assets/img/icon-mutual-fund.svg'
import useDashboard from "./useDashboard"
import LoadingScreen from "../../components/intermediate/loading-screen/LoadingScreen"
import { BannerButtonWithIllustration, BannerNAVUploaded } from "../../components/basics/banners/banner"

const DashboardView = () => {
    const {total, isLoading, getTotalInfo, setLoading, isNavUploaded} = useDashboard();
    return ( 
        <div className="view-container">
            {isLoading ? <LoadingScreen/> : <></>}
            <SideNavbar/>
            <div className="content-container w-80 padding-40">
                <div className="dashboard-property-bar">
                    <div className="flex-container flex-column gap-8">
                        <span className="h3-type color-grey">Welcome Back!</span>
                        <span className="body1-type color-extra-light-grey">Here’s what’s happening on our company</span>
                    </div>
                    <div className="flex-container-end flex-row gap-12">
                        <ButtonIcon imgSrc={Search}/>
                        <ButtonIcon imgSrc={Notification}/>
                    </div>
                </div>
                <div className="flex-container flex-column gap-24">
                    {isNavUploaded === "false" ? <BannerButtonWithIllustration/> : <BannerNAVUploaded/>}
                    <span className="h5-type color-light-grey">Business Summaries</span>
                    <div className="card-filter">
                        <div className="date-filter-group">
                            <span className="h6-type color-grey">Date Filter</span>
                            <ButtonIcon imgSrc="Reset" fnOnClick={() => window.location.reload()}/>
                        </div>
                        <div className="flex-container-2">
                            <CustomDateRangeInputs fnApplyFilter={getTotalInfo} fnOnChange={() => {}} />
                        </div>
                    </div>
                    <div className="flex-container flex-column gap-24">
                        <div className="flex-container flex-row gap-24">
                            <div className="dashboard-info-card">
                                <div className="dashboard-info-icon">
                                    <img src={Transaction}/>
                                </div>
                                <span className="h3-type color-grey">{total.totalTransactionSub.total_sub}</span>
                                <span className="body2-type color-light-grey">Total Sub Transactions</span>
                            </div>
                            <div className="dashboard-info-card">
                                <div className="dashboard-info-icon">
                                    <img src={Redeem}/>
                                </div>
                                <span className="h3-type color-grey">{total.totalTransactionRedeem.total_redeem}</span>
                                <span className="body2-type color-light-grey">Total Redeem Transactions</span>
                            </div>
                        </div>
                        <div className="flex-container flex-row gap-24">
                            <div className="dashboard-info-card">
                                <div className="dashboard-info-icon">
                                    <img src={Registrants}/>
                                </div>
                                <span className="h3-type color-grey">{total.totalRegistration.total_reg}</span>
                                <span className="body2-type color-light-grey">Total Registrations</span>
                            </div>
                            <div className="dashboard-info-card">
                                <div className="dashboard-info-icon">
                                    <img src={Products}/>
                                </div>
                                <span className="h3-type color-grey">{total.totalProduct.total_prod}</span>
                                <span className="body2-type color-light-grey">Total Products</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
 
export default DashboardView;