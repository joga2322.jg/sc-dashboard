import { useEffect, useLayoutEffect, useState } from "react"
import { useDeps } from "../../shared/DepContext"
import { INITIAL_VALUE } from "../../utils/constants"

const useDashboard = () => {
    const { dashboardService } = useDeps();
    const [total, setTotal] = useState(INITIAL_VALUE.DASHBOARD_INFO);
    const [isLoading, setLoading] = useState(false);
    const isNavUploaded = localStorage.getItem('isNavUploaded')

    useEffect(() => {
        getTotalInfo()
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
        }, 1500);
    }, [])

    useLayoutEffect(() => {
        if (isLoading) {
          document.body.style.overflow = "hidden";
          document.body.style.height = "100%";
        }
        if (!isLoading) {
          document.body.style.overflow = "auto";
          document.body.style.height = "auto";
        }
    }, [isLoading]);

    const getTotalInfo = async (dateRange = [null, null]) => {
        if (dateRange[0] && dateRange[1]){
            dateRange[0] = dateRange[0].toLocaleDateString('en-GB')
            dateRange[1] = dateRange[1].toLocaleDateString('en-GB')
        }

        try {
            const resp = await dashboardService.getDashboardInfo(dateRange[0], dateRange[1])
            setTotal(resp)
        } catch (error) {
            alert(error)
        }
    }

    return {
        total,
        isLoading,
        getTotalInfo,
        setLoading,
        isNavUploaded
    }
}

export default useDashboard