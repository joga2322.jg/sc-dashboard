import React from 'react'
import '../../assets/css/container.css'
import '../../assets/css/product-view.css'
import '../../assets/css/font-style.css'
import '../../assets/css/line-style.css'
import '../../assets/css/table-style.css'
import SideNavbar from '../../components/intermediate/side-navigation/SideNavbar'
import { Outlet } from 'react-router-dom'

const ProductView = () => {
  return (
    <div className='view-container'>
      <SideNavbar />
      <div className='content-container w-80 padding-40'>
        <Outlet />
      </div>
    </div>
  )
}

export default ProductView