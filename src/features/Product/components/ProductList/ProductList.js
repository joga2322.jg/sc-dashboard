import { ButtonIcon, ButtonPrimary } from "../../../../components/basics/buttons/Buttons"
import InfoIcon from "../../../../assets/img/icon-information.svg"
import EditIcon from "../../../../assets/img/icon-edit.svg"
import DeleteIcon from "../../../../assets/img/icon-garbage.svg"
import SearchIcon from "../../../../assets/img/icon-search.svg"
import NotifIcon from "../../../../assets/img/icon-notification.svg"
import useProductList from "./useProductList"
import DataTable from "react-data-table-component"
import LoadingScreen from "../../../../components/intermediate/loading-screen/LoadingScreen"

export const FilterComponent = ({ filterText, onFilter }) => (  
    <input className="search-product-input" id="search" type="text" placeholder="Search Fund Name" aria-label="Search Input" value={filterText} onChange={onFilter}/>
)

const ProductList = () => {
    const {filteredItems, subHeaderComponentMemo, onNavigate, onDeleteProduct, isLoading, setLoading} = useProductList()

    const tableColumns = [
        {
            name: 'Fund Code',
            selector: row => row.fund_code,
            sortable: true,
            width: '20%'
        },
        {
            name: 'Fund Name',
            selector: row => row.fund_name,
            sortable: true,
            wrap: true
        },
        {
            name: 'Last Updated Time',
            selector: row => row.updatedTime,
            sortable: true,
            width: '20%'
        },
        {
            name: 'Actions',
            button: true,
            width: '20%',
            cell: (product) => <div className="product-list-action-group">
                <ButtonIcon imgSrc={EditIcon} fnOnClick={() => onNavigate('edit', product.id)}/>
                <ButtonIcon imgSrc={DeleteIcon} fnOnClick={() => {
                    if (window.confirm("Are you sure want to delete?")) onDeleteProduct(product.id)}
                }/>
                <ButtonIcon imgSrc={InfoIcon} fnOnClick={() => onNavigate('detail', product.id)}/>
            </div>
        }
    ]

    const tableCustomStyles = {
        table: {style: {
            color: 'rgba(48, 53, 65, 0.87)'
        }},
        head: {style: {
            fontSize: '14px',
            lineHeight: '17px',
            fontWeight: 600,
            letterSpacing: '0.15px',
        }},
        headRow: {style: {
            backgroundColor: '#F9FAFC'
        }},
        cells: {style: {
            padding: '20px'
        }},
        rows: {style: {
            fontSize: '14px',
            lineHeight: '20px',
            fontWeight: 500,
            letterSpacing: '0.15px',
            color: 'rgba(48, 53, 65, 0.87)'
        }},
    }

    return (
        <>
            <div className='flex-container flex-row'>
            {isLoading ? <LoadingScreen/> : <></>}
                <div className="flex-container flex-column gap-8">
                    <span className="h3-type color-grey">Products</span>
                    <span className="body1-type color-extra-light-grey">Here’s where you can manage our products responsibly</span>
                </div>
                <div className="flex-container-end flex-row gap-12">
                    <ButtonIcon imgSrc={SearchIcon}/>
                    <ButtonIcon imgSrc={NotifIcon}/>
                </div>
            </div>
            <div className="product-container">
                <div className="flex-container flex-row" style={{justifyContent: 'space-between'}}>
                    <div className="w-80">
                        <span className="h4-type color-grey">Product List</span>
                    </div>
                    <div className="w-20">
                        <ButtonPrimary label="Add New Product" fnOnClick={() => onNavigate('add')} />
                    </div>
                </div>
                <div className="product-container white padding-24">
                    <div style={{display:"flex", flexDirection:"column", gap:"24px", width:"100%"}}>
                        <DataTable
                            columns={tableColumns}
                            data={filteredItems}
                            pagination
                            defaultSortFieldId={2}
                            customStyles={tableCustomStyles}
                            subHeader
                            subHeaderComponent={subHeaderComponentMemo}
                            persistTableHead
                        />
                    </div>
                </div>
            </div>
        </>
    )
}

export default ProductList