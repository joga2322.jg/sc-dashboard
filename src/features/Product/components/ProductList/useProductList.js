import { useEffect, useLayoutEffect, useMemo, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDeps } from "../../../../shared/DepContext"
import { FilterComponent } from "./ProductList";

const useProductList = () => {
    const navigate = useNavigate()
    const { productService } = useDeps();
    const [product, setProduct] = useState([]);
    const [filterText, setFilterText] = useState('');

    const [isLoading, setLoading] = useState(false);

    useEffect(() => {
        getProduct()
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
        }, 1500);
    }, [])

    useLayoutEffect(() => {
        if (isLoading) {
          document.body.style.overflow = "hidden";
          document.body.style.height = "100%";
        }
        if (!isLoading) {
          document.body.style.overflow = "auto";
          document.body.style.height = "auto";
        }
    }, [isLoading]);

	const filteredItems = product.filter(
		item => item.fund_name && item.fund_name.toLowerCase().includes(filterText.toLowerCase()),
	);

	const subHeaderComponentMemo = useMemo(() => {
		return (
			<FilterComponent onFilter={e => setFilterText(e.target.value)} filterText={filterText} />
		);
	}, [filterText]);

    const getProduct = async () => {
        try {
            const resp = await productService.getAllProduct()
            setProduct(resp)
        } catch (error) {
            alert(error)
        }
    }

    const onNavigate = (page, id) => {
        switch (page) {
            case 'edit':
                navigate(`/product/${id}/edit`)
                break;
            case 'detail':
                navigate(`/product/${id}`)
                break;
            case 'add':
                navigate('/product/new')
                break;
            default:
                break;
        }
    }

    const onDeleteProduct = async (id) => {
        try {
            await productService.deleteProductById(id)
            const resp = await productService.getAllProduct()
            setProduct(resp)
        } catch (error) {
            alert(error)
        }
    }
   

    return {
        filteredItems,
        subHeaderComponentMemo,
        onNavigate,
        onDeleteProduct,
        isLoading, 
        setLoading
    }
}

export default useProductList;