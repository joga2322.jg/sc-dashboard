import { useEffect, useLayoutEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useDeps } from "../../../../shared/DepContext"

const useDetailProduct = () => {
    const navigate = useNavigate()
    const params = useParams();
    const id = parseInt(params.id, 10);

    const { productService } = useDeps();
    const [product, setProduct] = useState({})

    const [isLoading, setLoading] = useState(false);

    useEffect(() => {
        getProduct()
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
        }, 1500);
    }, [])

    useLayoutEffect(() => {
        if (isLoading) {
          document.body.style.overflow = "hidden";
          document.body.style.height = "100%";
        }
        if (!isLoading) {
          document.body.style.overflow = "auto";
          document.body.style.height = "auto";
        }
    }, [isLoading]);

    const getProduct = async () => {
        try {
            const resp = await productService.getProductById(id)
            setProduct(resp)
        } catch (error) {
            alert(error)
        }
    }

    const onViewDocument = (link) => {
        window.open(link, '_blank')
    }

    const onNavigate = (page) => {
        if (page === 'edit'){
            navigate(`/product/${id}/edit`)
        }
    }

    const onDeleteProduct = async (id) => {
        try {
            await productService.deleteProductById(id)
            navigate(-1, { replace: true })
        } catch (error) {
            alert(error)
        }
    }

    return {
        product,
        onNavigate,
        onViewDocument,
        onDeleteProduct,
        isLoading, 
        setLoading
    }
}

export default useDetailProduct