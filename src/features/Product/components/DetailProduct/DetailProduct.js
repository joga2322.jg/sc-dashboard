import { ButtonIcon, ButtonPrimary } from "../../../../components/basics/buttons/Buttons"
import TransactionIcon from "../../../../assets/img/icon-transaction.svg"
import EditIcon from "../../../../assets/img/icon-edit.svg"
import DeleteIcon from "../../../../assets/img/icon-garbage.svg"
import useDetailProduct from "./useDetailProduct"
import LoadingScreen from "../../../../components/intermediate/loading-screen/LoadingScreen"

const DetailProduct = () => {
    const {product, onNavigate, onViewDocument, onDeleteProduct, isLoading, setLoading} = useDetailProduct()

    return (
        <>
            <div className='flex-container flex-row'>
                {isLoading ? <LoadingScreen/> : <></>}
                <div className="flex-container flex-column gap-8">
                    <p className='label-input color-light-grey'>
                    <a href='/product'>Product</a> / Product Detail
                    </p>
                    <span className="h3-type color-grey">Product Detail</span>
                    <span className="body1-type color-extra-light-grey">Here’s the complete information of a product</span>
                </div>
            </div>
            <div className="product-container flex-column">
                <div className="w-100 product-detail-card flex-row" style={{justifyContent: 'space-between'}}>
                    <div className="flex-container flex-column gap-8">
                        <div className="label-input"><span className="color-light-grey">Code:</span> <span className="color-grey">{product.fund_code}</span></div>
                        <div className="h5-type color-sc-blue-1">{product.fund_name}</div>
                        <div className="product-detail-group">
                            <div className="body2-type"><span className="color-light-grey">Prefix:</span> <span className="color-grey">{product.prefix}</span></div>
                            <div className="body2-type"><span className="color-light-grey">ID:</span> <span className="color-grey">{product.id}</span></div>
                        </div>
                    </div>
                    <div className="product-form-buttons-group" style={{alignSelf: 'center'}}>
                        <ButtonIcon imgSrc={EditIcon} fnOnClick={() => onNavigate('edit')}/>
                        <ButtonIcon imgSrc={DeleteIcon} fnOnClick={() => {
                            if (window.confirm("Are you sure want to delete?")) onDeleteProduct(product.id)}
                        }/>
                    </div>
                </div>
                <div className="product-detail-group">
                    <div className="w-50 product-detail-card flex-row">
                        <div className="icon-bg"><img src={TransactionIcon} alt="transaction icon"/></div>
                        <div className="flex-container flex-column gap-8">
                            <div className="label-input color-light-grey">Total Minimum Sub</div>
                            <div className="body1-semibold-type color-grey">{product.totalMinimalSubscribe}</div>
                        </div>
                    </div>
                    
                    <div className="w-50 product-detail-card flex-row">
                        <div className="icon-bg"><img src={TransactionIcon} alt="transaction icon"/></div>
                        <div className="flex-container flex-column gap-8">
                            <div className="label-input color-light-grey">Total Minimum Top Up</div>
                            <div className="body1-semibold-type color-grey">{product.totalMinimalTopup}</div>
                        </div>
                    </div>
                </div>
                <div className="w-100 product-detail-card flex-column">
                    <div className="product-detail-group">
                        <div className="w-20 flex-container flex-column gap-24">
                            <div className="body1-semibold-type color-grey">Allocation</div>
                            <div className="w-100 flex-column">
                                <div className="body2-semibold-type"><span className="color-light-grey">Stock - </span><span className="color-grey">{product.allocation_stock}</span>%</div>
                                <div className="progress-bar-bg">
                                    <div className="progress-bar-filled" style={{width: `${product.allocation_stock}%`}}></div>
                                </div>
                            </div>
                            <div className="w-100 flex-column">
                                <div className="body2-semibold-type"><span className="color-light-grey">Forex - </span><span className="color-grey">{product.allocation_forex}</span>%</div>
                                <div className="progress-bar-bg">
                                    <div className="progress-bar-filled" style={{width: `${product.allocation_forex}%`}}></div>
                                </div>
                            </div>
                            <div className="w-100 flex-column">
                                <div className="body2-semibold-type"><span className="color-light-grey">Obligation - </span><span className="color-grey">{product.allocation_obligation}</span>%</div>
                                <div className="progress-bar-bg">
                                    <div className="progress-bar-filled" style={{width: `${product.allocation_obligation}%`}}></div>
                                </div>
                            </div>
                        </div>
                        <div className="w-80 flex-container flex-column gap-24">
                            <div className="body1-semibold-type color-grey">Documents</div>
                            <div className="documents-box">
                                <div>
                                    <div className="label-input color-light-grey" style={{marginBottom: '12px'}}>Fund Fact ID</div>
                                    <div className="body2-semibold-type color-grey">{product.fundFactSheet_id}</div>
                                </div>
                                <div>
                                    <ButtonPrimary label="View Fund Fact Sheet" fnOnClick={() => onViewDocument(product.fundFactSheetUrl)} />
                                </div>
                            </div>
                            <div className="documents-box">
                                <div>
                                    <div className="label-input color-light-grey" style={{marginBottom: '12px'}}>Prospectus ID</div>
                                    <div className="body2-semibold-type color-grey">{product.prospectus_id}</div>
                                </div>
                                <div>
                                    <ButtonPrimary label="View Prospectus" fnOnClick={() => onViewDocument(product.prospectusUrl)} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="label-input">
                    <span className="color-light-grey">Last Updated At:</span> <span className="color-grey">{product.updatedTime}</span>
                </div>
            </div>
        </>
    )
}

export default DetailProduct