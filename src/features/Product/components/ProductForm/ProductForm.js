import React from 'react'
import { NumberInput, TextInput, CurrecyInput,FileUploaderForProduct } from '../../../../components/basics/inputs/Inputs'
import { ButtonPrimary, ButtonSecondary } from '../../../../components/basics/buttons/Buttons'
import useProductForm from './useProductForm'
import LoadingScreen from '../../../../components/intermediate/loading-screen/LoadingScreen'

const ProductForm = () => {
    const { 
        onInputChange,
        product,
        currency,
        errMsg,
        onNavigate,
        onSubmitCreateProduct,
        isLoading,
        setProduct
    } = useProductForm()
    return (
        <>
            <div className='flex-container flex-row'>
                {isLoading ? <LoadingScreen/> : <></>}
                <div className="flex-container flex-column gap-8">
                    <p className='label-input color-light-grey'>
                        <a href='/product'>Product</a> / Add Product
                    </p>
                    <span className="h3-type color-grey">Add Product</span>
                    <span className="body1-type color-extra-light-grey">Here’s where you can add new product responsibly</span>
                </div>
            </div>
            <div className='product-container white padding-24'>
                <div className='flex-container flex-column gap-24'>
                    <h5 className='h5-type color-sc-blue-2'>Step 1: General Info</h5>
                    <div className='product-form-group flex-row'>
                        <TextInput label='Prefix - Required' name='prefix' fnOnChange={onInputChange} errorMsg={errMsg.prefixErr} defVal={product.prefix} />
                        <TextInput label='Fund Code - Required' name='fund_code' fnOnChange={onInputChange} errorMsg={errMsg.fundCodeErr} defVal={product.fund_code} />
                    </div>
                    <TextInput label='Fund Name - Required' name='fund_name' fnOnChange={onInputChange} errorMsg={errMsg.fundNameErr}/>
                    <div className='product-form-group flex-row'>
                        <CurrecyInput label='Total Minimum Sub - Required' name='totalMinimalSubscribe' fnOnChange={onInputChange} errorMsg={errMsg.totalMinSubsErr} value={product.totalMinimalSubscribe} currencyVal={currency.totalMinimalSubscribe}/>
                        <CurrecyInput label='Total Minimum Top Up - Required' name='totalMinimalTopup' fnOnChange={onInputChange} errorMsg={errMsg.totalMinTopupErr} value={product.totalMinimalTopup} currencyVal={currency.totalMinimalTopup}/>
                    </div>
                </div>
                <div className='linear-gradient'></div>
                <div className='flex-container flex-column gap-20'>
                    <h5 className='h5-type color-sc-blue-2'>Step 2: Fund Documents</h5>
                    <div className='product-form-group flex-row'>
                        <div className='product-form-group flex-column'>
                            <TextInput label='Fund Fact Sheet ID - Required' name='fundFactSheet_id' fnOnChange={onInputChange} errorMsg={errMsg.factSheetIdErr} defVal={product.fundFactSheet_id} />
                            {/* <TextInput label='Fund Fact Sheet URL - Required' name='fundFactSheetUrl' fnOnChange={onInputChange} errorMsg={errMsg.factSheetUrlErr} defVal={product.fundFactSheetUrl} /> */}
                            <FileUploaderForProduct label='Fund Fact Sheet File - Required' file={product.fundFactSheetFile} setFile={setProduct} errorMsg={errMsg.fundFactSheetFileErr} fileName="fundFactSheetFile"/>
                        </div>
                        <div className='product-form-group flex-column'>
                            <TextInput label='Prospectus ID - Required' name='prospectus_id' fnOnChange={onInputChange} errorMsg={errMsg.prospectusIdErr} defVal={product.prospectus_id} />
                            {errMsg.fundFactSheetFileErr === "" ?
                            <FileUploaderForProduct label='Prospectus File - Required' file={product.prospectusFile} setFile={setProduct} errorMsg={errMsg.prospectusFileErr} fileName="prospectusFile"/>:
                            <FileUploaderForProduct label='Prospectus File - Required' file={product.prospectusFile} setFile={setProduct} errorMsg={errMsg.prospectusFileErr} fileName="prospectusFile" disable="true"/>
                            }
                            
                        </div>  
                    </div>
                </div>
                <div className='linear-gradient'></div>
                <div className='flex-container flex-column gap-20'>
                    <h5 className='h5-type color-sc-blue-2'>Step 3: Allocation</h5>
                    <div className='product-form-group flex-row'>
                        <NumberInput label='Stock Allocation - Required' name='allocation_stock' fnOnChange={onInputChange} errorMsg={errMsg.allocStockErr} defVal={product.allocation_stock} value={product.allocation_stock} />
                        <NumberInput label='Forex Allocation - Required' name='allocation_forex' fnOnChange={onInputChange} errorMsg={errMsg.allocForexErr} defVal={product.allocation_forex} value={product.allocation_stock}/>
                        <NumberInput label='Obligation Allocation - Required' name='allocation_obligation' fnOnChange={onInputChange} errorMsg={errMsg.allocObligationErr} defVal={product.allocation_obligation} />
                    </div>
                </div>
                <div className='linear-gradient'></div>
                <div className='product-form-buttons-group' style={{alignSelf: 'flex-end'}}>
                    <ButtonSecondary label='Cancel' fnOnClick={onNavigate}/>
                    <ButtonPrimary label='Submit' fnOnClick={onSubmitCreateProduct}/>
                </div>
            </div>
        </>
    )
}

export default ProductForm