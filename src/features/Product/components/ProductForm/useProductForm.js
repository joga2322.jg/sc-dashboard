import { useEffect, useLayoutEffect, useState } from 'react';
import { newFieldRequiredError, newFieldValueError, newInvalidPrefixLength, newInvalidTotalAllocation, newMustFilledFirstError } from '../../../../utils/error';
import { INITIAL_VALUE } from '../../../../utils/constants';
import { useDeps } from '../../../../shared/DepContext'
import { useNavigate } from 'react-router-dom';

const useProductForm = () => {
    const navigate = useNavigate()
    const { productService } = useDeps();
    const [product, setProduct] = useState(INITIAL_VALUE.PRODUCT)
    const [currency, setCurrency] = useState(INITIAL_VALUE.CUR_VALUE)
    const [errMsg, setErrMsg] = useState(INITIAL_VALUE.ERR_PRODUCT)
    const [isLoading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
        }, 1500);
    }, [])

    useEffect(()=>{
        console.log(product);
    }, [product])

    useLayoutEffect(() => {
        if (isLoading) {
          document.body.style.overflow = "hidden";
          document.body.style.height = "100%";
        }
        if (!isLoading) {
          document.body.style.overflow = "auto";
          document.body.style.height = "auto";
        }
    }, [isLoading]);

    const onInputChange = (key, value) => {
        setProduct({
            ...product,
            [key]: value
        })
    }

    const onNavigate = () => {
        navigate('/product', { replace: true })
    }

    const onSubmitCreateProduct = async () => {
        if (JSON.stringify(errMsg) === JSON.stringify(INITIAL_VALUE.ERR_PRODUCT)){
            setLoading(true)
            try {
                await productService.addProduct(product)
                alert("Success")
                onNavigate()
            } catch (error) {
                alert(error)
            } finally {
                setLoading(false)
            }
        } else{
            alert("Please input the valid fields")
        }
    }

    const sumAllocation = () => {
        return parseFloat(product.allocation_forex) + parseFloat(product.allocation_obligation) + parseFloat(product.allocation_stock)
    }

    useEffect(() => {
        if (!product.fund_code){
            setErrMsg(prevState => ({...prevState, fundCodeErr: newFieldRequiredError("fund code")}))
        } else setErrMsg(prevState => ({...prevState, fundCodeErr: ""}))
    }, [product.fund_code])

    useEffect(() => {
        if (!product.fund_name){
            setErrMsg(prevState => ({...prevState, fundNameErr: newFieldRequiredError("fund name")}))
        } else setErrMsg(prevState => ({...prevState, fundNameErr: ""}))
    }, [product.fund_name])

    useEffect(() => {
        setCurrency({...currency, "totalMinimalSubscribe": new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(product.totalMinimalSubscribe)})
        if (!product.totalMinimalSubscribe || product.totalMinimalSubscribe === "0"){
            setErrMsg(prevState => ({...prevState, totalMinSubsErr: newFieldRequiredError("total minimum subscribe")}))
        } else if (/^\d+$/.test(product.totalMinimalSubscribe) === false){
            setProduct({...product, "totalMinimalSubscribe": ""})
            setErrMsg(prevState => ({...prevState, totalMinSubsErr: newFieldValueError("total minimum subscribe")}))
        }else setErrMsg(prevState => ({...prevState, totalMinSubsErr: ""}))
    }, [product.totalMinimalSubscribe])

    useEffect(() => {
        setCurrency({...currency, "totalMinimalTopup": new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(product.totalMinimalTopup)})
        if (!product.totalMinimalTopup || product.totalMinimalTopup === "0"){
            setErrMsg(prevState => ({...prevState, totalMinTopupErr: newFieldRequiredError("total minimum top up")}))
        } else if (/^\d+$/.test(product.totalMinimalTopup) === false){
            setProduct({...product, "totalMinimalTopup": ""})
            setErrMsg(prevState => ({...prevState, totalMinTopupErr: newFieldValueError("total minimum top up")}))
        } else setErrMsg(prevState => ({...prevState, totalMinTopupErr: ""}))
    }, [product.totalMinimalTopup])

    useEffect(() => {
        if (!product.fundFactSheet_id){
            setErrMsg(prevState => ({...prevState, factSheetIdErr: newFieldRequiredError("fund fact sheet id")}))
        } else setErrMsg(prevState => ({...prevState, factSheetIdErr: ""}))
    }, [product.fundFactSheet_id])

    //file upload checker
    useEffect(() => {
        if (product.fundFactSheetFile !== "" && product.prospectusFile === ""){
            setErrMsg(prevState => ({...prevState, prospectusFileErr: newFieldRequiredError("prospectus file")}))
        } else setErrMsg(prevState => ({...prevState, prospectusFileErr: ""}))
    }, [product.prospectusFile, product.fundFactSheetFile])

    useEffect(() => {
        if (product.fundFactSheetFile === ""){
            setErrMsg(prevState => ({...prevState, fundFactSheetFileErr: newFieldRequiredError("fund fact sheet file"), prospectusFileErr: newMustFilledFirstError("fund fact sheet file")}))
        } else setErrMsg(prevState => ({...prevState, fundFactSheetFileErr: ""}))
    }, [product.fundFactSheetFile, product.prospectusFile])
    //file upload checker

    // useEffect(() => {
    //     if (!product.fundFactSheetUrl){
    //         setErrMsg(prevState => ({...prevState, factSheetUrlErr: newFieldRequiredError("fund fact sheet url")}))
    //     } else setErrMsg(prevState => ({...prevState, factSheetUrlErr: ""}))
    // }, [product.fundFactSheetUrl])

    useEffect(() => {
        if (!product.prospectus_id){
            setErrMsg(prevState => ({...prevState, prospectusIdErr: newFieldRequiredError("prospectus id")}))
        } else setErrMsg(prevState => ({...prevState, prospectusIdErr: ""}))
    }, [product.prospectus_id])

    // useEffect(() => {
    //     if (!product.prospectusUrl){
    //         setErrMsg(prevState => ({...prevState, prospectusUrlErr: newFieldRequiredError("prospectus url")}))
    //     } else setErrMsg(prevState => ({...prevState, prospectusUrlErr: ""}))
    // }, [product.prospectusUrl])

    useEffect(() => {
        if (sumAllocation() !== 100) {
            setErrMsg(prevState => ({
                ...prevState,
                allocStockErr: newInvalidTotalAllocation(),
                allocForexErr: newInvalidTotalAllocation(),
                allocObligationErr: newInvalidTotalAllocation()
            }))
        } else setErrMsg(prevState => ({
            ...prevState,
            allocStockErr: "",
            allocForexErr: '',
            allocObligationErr: ''
        }))
    }, [product.allocation_forex, product.allocation_obligation, product.allocation_stock])

    useEffect(() => {
        if (!product.prefix){
            setErrMsg(prevState => ({...prevState, prefixErr: newFieldRequiredError("prefix")}))
        } else if (product.prefix.length > 2) {
            setErrMsg(prevState => ({...prevState, prefixErr: newInvalidPrefixLength()}))
        } else setErrMsg(prevState => ({...prevState, prefixErr: ""}))
    }, [product.prefix])

    return {
        onInputChange,
        product,
        currency,
        errMsg,
        onNavigate,
        onSubmitCreateProduct,
        isLoading,
        setProduct
    }
}

export default useProductForm