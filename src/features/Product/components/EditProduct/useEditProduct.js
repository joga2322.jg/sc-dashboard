import { useEffect, useLayoutEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useDeps } from "../../../../shared/DepContext";
import { INITIAL_VALUE } from "../../../../utils/constants";
import { newFieldRequiredError, newFieldValueError, newInvalidTotalAllocation } from '../../../../utils/error';

const useEditProduct = () => {
    const params = useParams();
    const id = parseInt(params.id, 10);
    const navigate = useNavigate()

    const { productService } = useDeps();
    const [product, setProduct] = useState(INITIAL_VALUE.PRODUCT)
    const [currency, setCurrency] = useState(INITIAL_VALUE.CUR_VALUE)
    const [errMsg, setErrMsg] = useState(INITIAL_VALUE.ERR_PRODUCT)

    const [isLoading, setLoading] = useState(false);

    useEffect(() => {
        getProductById()
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
        }, 1500);
    }, [])

    useLayoutEffect(() => {
        if (isLoading) {
          document.body.style.overflow = "hidden";
          document.body.style.height = "100%";
        }
        if (!isLoading) {
          document.body.style.overflow = "auto";
          document.body.style.height = "auto";
        }
    }, [isLoading]);

    const getProductById = async () => {
        try {
            const resp = await productService.getProductById(id)
            resp.totalMinimalSubscribe = resp.totalMinimalSubscribe.toString()
            resp.totalMinimalTopup = resp.totalMinimalTopup.toString()
            resp.allocation_forex = resp.allocation_forex.toString()
            resp.allocation_stock = resp.allocation_stock.toString()
            resp.allocation_obligation = resp.allocation_obligation.toString()
            setProduct(resp)
        } catch (error) {
            alert(error)
        }
    }

    const onNavigate = () => {
        navigate(-1, { replace: true })
    }

    const updateProduct = async () => {
        if (JSON.stringify(errMsg) === JSON.stringify(INITIAL_VALUE.ERR_PRODUCT)){
            setLoading(true)
            try {
                await productService.updateProductById(id, product)
                alert("Success")
                onNavigate()
            } catch (error) {
                alert(error)
            } finally{
                setLoading(false)
            }
        } else{
            alert("Please input the valid fields")
        }
    }

    const onInputChange = (key, value) => {
        setProduct({
            ...product,
            [key]: value
        })
    }

    const sumAllocation = () => {
        return parseFloat(product.allocation_forex) + parseFloat(product.allocation_obligation) + parseFloat(product.allocation_stock)
    }

    useEffect(() => {
        if (!product.fund_name){
            setErrMsg(prevState => ({...prevState, fundNameErr: newFieldRequiredError("fund name")}))
        } else setErrMsg(prevState => ({...prevState, fundNameErr: ""}))
    }, [product.fund_name])

    useEffect(() => {
        setCurrency({...currency, "totalMinimalSubscribe": new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(product.totalMinimalSubscribe)})
        if (!product.totalMinimalSubscribe){
            setErrMsg(prevState => ({...prevState, totalMinSubsErr: newFieldRequiredError("total minimum subscribe")}))
        } else if (/^\d+$/.test(product.totalMinimalSubscribe) === false){
            setProduct({...product, "totalMinimalSubscribe": ""})
            setErrMsg(prevState => ({...prevState, totalMinSubsErr: newFieldValueError("total minimum subscribe")}))
        }else setErrMsg(prevState => ({...prevState, totalMinSubsErr: ""}))
    }, [product.totalMinimalSubscribe])

    useEffect(() => {
        setCurrency({...currency, "totalMinimalTopup": new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(product.totalMinimalTopup)})
        if (!product.totalMinimalTopup){
            setErrMsg(prevState => ({...prevState, totalMinTopupErr: newFieldRequiredError("total minimum top up")}))
        } else if (/^\d+$/.test(product.totalMinimalTopup) === false){
            setProduct({...product, "totalMinimalTopup": ""})
            setErrMsg(prevState => ({...prevState, totalMinTopupErr: newFieldValueError("total minimum top up")}))
        } else setErrMsg(prevState => ({...prevState, totalMinTopupErr: ""}))
    }, [product.totalMinimalTopup])

    useEffect(() => {
        if (!product.fundFactSheet_id){
            setErrMsg(prevState => ({...prevState, factSheetIdErr: newFieldRequiredError("fund fact sheet id")}))
        } else setErrMsg(prevState => ({...prevState, factSheetIdErr: ""}))
    }, [product.fundFactSheet_id])

    useEffect(() => {
        if (!product.fundFactSheetUrl){
            setErrMsg(prevState => ({...prevState, factSheetUrlErr: newFieldRequiredError("fund fact sheet url")}))
        } else setErrMsg(prevState => ({...prevState, factSheetUrlErr: ""}))
    }, [product.fundFactSheetUrl])

    useEffect(() => {
        if (!product.prospectus_id){
            setErrMsg(prevState => ({...prevState, prospectusIdErr: newFieldRequiredError("prospectus id")}))
        } else setErrMsg(prevState => ({...prevState, prospectusIdErr: ""}))
    }, [product.prospectus_id])

    useEffect(() => {
        if (!product.prospectusUrl){
            setErrMsg(prevState => ({...prevState, prospectusUrlErr: newFieldRequiredError("prospectus url")}))
        } else setErrMsg(prevState => ({...prevState, prospectusUrlErr: ""}))
    }, [product.prospectusUrl])

    useEffect(() => {
        if (sumAllocation() !== 100) {
            setErrMsg(prevState => ({
                ...prevState,
                allocStockErr: newInvalidTotalAllocation(),
                allocForexErr: newInvalidTotalAllocation(),
                allocObligationErr: newInvalidTotalAllocation()
            }))
        } else setErrMsg(prevState => ({
            ...prevState,
            allocStockErr: "",
            allocForexErr: '',
            allocObligationErr: ''
        }))
    }, [product.allocation_forex, product.allocation_obligation, product.allocation_stock])

    return {
        product,
        errMsg,
        currency,
        updateProduct,
        onInputChange,
        onNavigate,
        isLoading, 
    }
}

export default useEditProduct