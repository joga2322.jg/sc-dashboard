import { CurrecyInput, NumberInput, TextInput } from "../../../../components/basics/inputs/Inputs";
import { ButtonPrimary, ButtonSecondary } from "../../../../components/basics/buttons/Buttons";
import useEditProduct from "./useEditProduct";
import LoadingScreen from "../../../../components/intermediate/loading-screen/LoadingScreen";

const EditProduct = () => {
    const { product, errMsg, currency, onInputChange, updateProduct, onNavigate, isLoading} = useEditProduct()
    const urlDetail = `/product/${product.id}`

    return (
        <>
            <div className='flex-container flex-row'>
            {isLoading ? <LoadingScreen/> : <></>}
                <div className="flex-container flex-column gap-8">
                    <p className='label-input color-light-grey'>
                        <a href='/product'>Product</a> / <a href={urlDetail}>Product Detail</a> / Edit Product
                    </p>
                    <span className="h3-type color-grey">Edit Product</span>
                    <span className="body1-type color-extra-light-grey">Here’s where you can edit product details responsibly</span>
                </div>
            </div>
            <div className='product-container white padding-24'>
                <div className='flex-container flex-column gap-20'>
                    <h5 className='h5-type color-sc-blue-2'>Step 1: General Info</h5>
                    <div className='product-form-group flex-row'>
                        <TextInput label='Prefix' name='prefix' fnOnChange={onInputChange} defVal={product.prefix} readOnly={true}/>
                        <TextInput label='Fund Code' name='fund_code' fnOnChange={onInputChange} defVal={product.fund_code} readOnly={true}/>
                    </div>
                    <TextInput label='Fund Name - Required' name='fund_name' fnOnChange={onInputChange} errorMsg={errMsg.fundNameErr} defVal={product.fund_name}/>
                    <div className='product-form-group flex-row'>
                        <CurrecyInput label='Total Minimum Sub - Required' name='totalMinimalSubscribe' fnOnChange={onInputChange} errorMsg={errMsg.totalMinSubsErr} value={product.totalMinimalSubscribe} currencyVal={currency.totalMinimalSubscribe}/>
                        <CurrecyInput label='Total Minimum Top Up - Required' name='totalMinimalTopup' fnOnChange={onInputChange} errorMsg={errMsg.totalMinTopupErr} value={product.totalMinimalTopup} currencyVal={currency.totalMinimalTopup}/>
                    </div>
                </div>
                <div className='linear-gradient'></div>
                <div className='flex-container flex-column gap-20'>
                    <h5 className='h5-type color-sc-blue-2'>Step 2: Fund Documents</h5>
                    <div className='product-form-group flex-row'>
                        <div className='product-form-group flex-column'>
                            <TextInput label='Fund Fact Sheet ID - Required' name='fundFactSheet_id' fnOnChange={onInputChange} errorMsg={errMsg.factSheetIdErr} defVal={product.fundFactSheet_id}/>
                            <TextInput label='Fund Fact Sheet URL - Required' name='fundFactSheetUrl' fnOnChange={onInputChange} errorMsg={errMsg.factSheetUrlErr} defVal={product.fundFactSheetUrl}/>
                        </div>
                        <div className='product-form-group flex-column'>
                            <TextInput label='Prospectus ID - Required' name='prospectus_id' fnOnChange={onInputChange} errorMsg={errMsg.prospectusIdErr} defVal={product.prospectus_id}/>
                            <TextInput label='Prospectus URL - Required' name='prospectusUrl' fnOnChange={onInputChange} errorMsg={errMsg.prospectusUrlErr} defVal={product.prospectusUrl}/>
                        </div>
                    </div>
                </div>
                <div className='linear-gradient'></div>
                <div className='flex-container flex-column gap-20'>
                    <h5 className='h5-type color-sc-blue-2'>Step 3: Allocation</h5>
                    <div className='product-form-group flex-row'>
                        <NumberInput label='Stock Allocation - Required' name='allocation_stock' fnOnChange={onInputChange} errorMsg={errMsg.allocStockErr} defVal={product.allocation_stock}/>
                        <NumberInput label='Forex Allocation - Required' name='allocation_forex' fnOnChange={onInputChange} errorMsg={errMsg.allocForexErr} defVal={product.allocation_forex}/>
                        <NumberInput label='Obligation Allocation - Required' name='allocation_obligation' fnOnChange={onInputChange} errorMsg={errMsg.allocObligationErr} defVal={product.allocation_obligation}/>
                    </div>
                </div>
                <div className='linear-gradient'></div>
                <div className="flex-container flex-row" style={{justifyContent: 'space-between', alignItems: 'center'}}>
                    <div className="flex-column label-input">
                        <span className="color-light-grey">Last Updated At:</span> <span className="color-grey">{product.updatedTime}</span>
                    </div>
                    <div className="flex-column">
                        <div className='product-form-buttons-group'>
                            <ButtonSecondary label='Cancel' fnOnClick={onNavigate}/>
                            <ButtonPrimary label='Submit' fnOnClick={updateProduct}/>
                        </div>
                    </div>
                    
                </div>
            </div>
        </>
    )
};

export default EditProduct;