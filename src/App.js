import './App.css';
import LoginView from './features/Login/LoginView';
import DashboardView from './features/Dashboard/DashboardView';
import ProductView from './features/Product/ProductView';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import ProtectedRoute from './navigation/ProtectedRoute';
import UploadView from './features/Upload/UploadView';
import DetailProduct from './features/Product/components/DetailProduct/DetailProduct';
import ProductList from './features/Product/components/ProductList/ProductList';
import EditProduct from './features/Product/components/EditProduct/EditProduct';
import ProductForm from './features/Product/components/ProductForm/ProductForm';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<LoginView/>}/>
          <Route element={<ProtectedRoute/>}>
            <Route path="dashboard" element={<DashboardView/>}/>
            <Route path="product" element={<ProductView/>}>
              <Route index element={<ProductList/>}/>
              <Route path="new" element={<ProductForm />}/>
              <Route path=":id" element={<DetailProduct />}/>
              <Route path=":id/edit" element={<EditProduct />}/>
            </Route>
            <Route path="upload" element={<UploadView/>}/>
          </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
