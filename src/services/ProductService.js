const ProductService = ({doPost, doGet, doPut, doDelete}) => {
    const addProduct = async (newProduct) => {
        try {
            const response = await doPost({url: '/product/', data: newProduct})
            return response
        } catch (error) {
            throw error
        }
    }

    const getProductById = async (id) => {
        try {
            const resp = await doGet({url: `/product/${id}`})

            const defaultResp = ["responseCode", "responseMessage"]
            const respArr = Object.entries(resp);
            const filteredArr = respArr.filter(function ([key, value]) {
                return !defaultResp.includes(key);
            });
            
            return Object.fromEntries(filteredArr);
        } catch (error) {
            throw error
        }
    }

    const updateProductById = async (id, product) => {
        try {
            const response = await doPut({url: `/product/${id}`, data: product})
            return response
        } catch (error) {
            throw error
        }
    }

    const getAllProduct = async () => {
        try {
            const resp = await doGet({url: '/product/'})
            return resp.products;
        } catch (error) {
            throw error
        }
    }

    const deleteProductById = async (id) => {
        try {
            const resp = await doDelete({url: `/product/${id}`})
            return resp
        } catch (error) {
            throw error
        }
    }


    return {
        addProduct,
        getProductById,
        updateProductById,
        getAllProduct,
        deleteProductById
    }
}

export default ProductService