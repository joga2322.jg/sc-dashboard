const LoginService = ({doPost}) => {
    const loginAdmin = async (admin) => {
        try {
            const response = await doPost({url: '/login/', data: admin})
            return response
        } catch (error) {
            throw error
        }
    }

    return {
        loginAdmin
    }
}

export default LoginService