const DashboardService = ({doGet}) => {
    const getDashboardInfo = async (dt_start="", dt_end="") => {
        try {
            let response;
            if (dt_start && dt_end){
                response = await doGet({url: `/dashboard/?dt_start=${dt_start}&dt_end=${dt_end}`})
            } else {
                response = await doGet({url: '/dashboard/'})
            }

            const defaultResp = ["responseCode", "responseMessage"]
            const respArr = Object.entries(response);
            const filteredArr = respArr.filter(function ([key, value]) {
                return !defaultResp.includes(key);
            });
            
            return Object.fromEntries(filteredArr);
        } catch (error) {
            throw error
        }
    }
    return {
        getDashboardInfo
    }
}

export default DashboardService