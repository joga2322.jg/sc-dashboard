const UploadService = ({doPost}) => {
    const uploadRegistration = async (file) => {
        try {
            const response = await doPost({url: '/upload/upload_registration', data: file})
            return response
        } catch (error) {
            throw error
        }
    }

    const uploadTransaction = async (file) => {
        try {
            const response = await doPost({url: '/upload/upload_transaction', data: file})
            return response
        } catch (error) {
            throw error
        }
    }

    const uploadUserBalance = async (file) => {
        try {
            const response = await doPost({url: '/upload/upload_user_balance', data: file})
            return response
        } catch (error) {
            throw error
        }
    }

    const uploadNav = async (file) => {
        try {
            const response = await doPost({url: '/upload/upload_nav', data: file})
            return response
        } catch (error) {
            throw error
        }
    }


    return {
        uploadRegistration,
        uploadTransaction,
        uploadUserBalance,
        uploadNav
    }
}

export default UploadService