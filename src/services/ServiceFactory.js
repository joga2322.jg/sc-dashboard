import LoginService from "./LoginService"
import ProductService from "./ProductService"
import DashboardService from "./DashboardService"
import UploadService from "./UploadService"

const ServiceFactory = (apiClient) => {
    return {
        productService: ProductService(apiClient),
        loginService: LoginService(apiClient),
        dashboardService: DashboardService(apiClient),
        uploadService: UploadService(apiClient)
    }
}

export default ServiceFactory
